import sqlite3
from typing import List, Optional

import fields


class sql_sandbox:
    connection: Optional[sqlite3.Connection]
    cursor: Optional[sqlite3.Cursor]
    return_id: bool

    def __init__(self, return_id: bool = False):
        self.connection = None
        self.cursor = None
        self.return_id = return_id

    def setup(self):
        self.connection = sqlite3.connect("db")
        self.cursor = self.connection.cursor()

    def shutdown(self):
        self.connection.commit()
        self.connection.close()

    def execute(self, query: str) -> Optional[int]:
        self.cursor.execute(query)
        if self.return_id:
            return self.cursor.lastrowid

    def __enter__(self):
        self.setup()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.shutdown()


def create_table_from_class(cls):
    table_name = cls.__name__
    sql = f"CREATE TABLE IF NOT EXISTS {table_name}" + "(" + "\n"
    model_fields = [
        field for field in cls.__dict__.values() if isinstance(field, fields.Field)
    ]

    sql += ",".join(field.sql_column_declaration for field in model_fields)
    sql += ");"
    with sql_sandbox() as sandbox:
        sandbox.execute(sql)


def save_instance_to_database(instance):
    model_fields: List[fields.Field] = [
        field
        for field in type(instance).__dict__.values()
        if isinstance(field, fields.Field) and field.name != "id"
    ]
    table_name = type(instance).__name__
    column_names = ",".join([field.name for field in model_fields])
    column_values = ",".join(
        [field.to_sql(getattr(instance, field.name)) for field in model_fields]
    )
    sql = (
        f"INSERT INTO {table_name} ({column_names}) VALUES ({column_values});"
    )
    with sql_sandbox(return_id=True) as sandbox:
        instance.id = sandbox.execute(sql)


def update_instance_to_database(instance):
    model_fields: List[fields.Field] = [
        field
        for field in type(instance).__dict__.values()
        if isinstance(field, fields.Field) and field.name != "id"
    ]
    table_name = type(instance).__name__
    set_clause = ",".join(
        (f"{field.name} = {field.to_sql(getattr(instance, field.name))}" for field in model_fields)
    )
    where_clause = f"WHERE id = {instance.id}"
    sql = (
        f"UPDATE {table_name} SET {set_clause} {where_clause}"
    )
    with sql_sandbox() as sandbox:
        sandbox.execute(sql)
