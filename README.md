Firstly, import necessary classes:

```python
from om import Model
from fields import TextField, IntegerField, JSONField
```

Then, create class like this:

```python
class Person(Model):
    first_name = TextField()
    last_name = TextField()
    salary = IntegerField()
    settings = JSONField()
```

and run the script.

Effects:
- sqlite database will be created with name 'db.db'
- database will containt 4 columns  
  - id: (integer, autoincrementing)
  - first_name: (text field)
  - last_name: (text field)
  - salary: (integer field)
  - settings: (json field)


(so far only TextField and IntegerField are available)


Lets go on and create instance of Person class:
```python
person = Person(
    first_name="John",
    last_name="Smith",
    salary=32000,
    settings={"timezone": "UTC+02:00"}
)
person.save()
``` 

After calling `save()` method, table 'Person' will create new record with data from the instance.


One can easily update record, by modifying attribute and calling `save()` again:

```python
person.salary += 3000
person.save()
```

To be continued.
