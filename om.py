from typing import Optional

import fields
from helpers import create_table_from_class, save_instance_to_database, update_instance_to_database


class MetaModel(type):
    def __new__(mcs, name: str, bases: tuple, methods: dict):
        methods["id"] = fields.IndexField()
        return super().__new__(mcs, name, bases, methods)


class Model(metaclass=MetaModel):
    id: Optional[int]

    def __init__(self, **kwargs):
        kwargs.update({"id": None})
        for field in type(self).__dict__.values():
            if isinstance(field, fields.Field):
                setattr(self, field.name, kwargs[field.name])

    def __init_subclass__(cls, **kwargs):
        for name, field in cls.__dict__.items():
            if isinstance(field, fields.Field):
                field.name = name
        create_table_from_class(cls)
        return super().__init_subclass__()

    def save(self):
        if self.id is None:
            save_instance_to_database(instance=self)
        else:
            update_instance_to_database(instance=self)
