import abc
import json


class Field(abc.ABC):
    name: str

    def __new__(cls, *args, **kwargs):
        if cls is Field:
            raise TypeError("cannot instantiate")
        return super().__new__(cls)

    @property
    @abc.abstractmethod
    def sql_column_declaration(self):
        ...

    @abc.abstractmethod
    def to_sql(self, value):
        ...

    def __set__(self, instance, value):
        instance.__dict__[self.name] = value

    def __get__(self, instance, owner):
        return instance.__dict__[self.name]


class IndexField(Field):
    def __init__(self):
        self.name = "id"
        self.null = False

    @property
    def sql_column_declaration(self):
        return "id INTEGER PRIMARY KEY AUTOINCREMENT"

    def to_sql(self, value):
        pass


class TextField(Field):
    def __init__(self, null: bool = False):
        self.name = None
        self.null = null

    @property
    def sql_column_declaration(self):
        null = "NOT NULL" if self.null else ""
        return f"{self.name} TEXT {null}"

    def to_sql(self, value):
        return repr(value)


class IntegerField(Field):
    def __init__(self, null: bool = False):
        self.name = None
        self.null = null

    @property
    def sql_column_declaration(self):
        null = "NOT NULL" if self.null else ""
        return f"{self.name} INTEGER {null}"

    def to_sql(self, value):
        return repr(value)


class JSONField(Field):
    def __init__(self):
        self.name = None

    @property
    def sql_column_declaration(self):
        return f"{self.name} TEXT NOT NULL"

    def __set__(self, instance, value: dict):
        instance.__dict__[self.name] = json.dumps(value)

    def __get__(self, instance, owner):
        return json.loads(instance.__dict__[self.name])

    def to_sql(self, value):
        return repr(json.dumps(value))
